package itis.parsing2;

/*
Вы пишите систему для интеграции с готовой структурой заказчика и обработки данных о фабриках корпораций.
!!КЛАСС FACTORY и классы ошибок менять НЕЛЬЗЯ (совсем). Считается что они являются частью готовой библиотеки!!

 * В папке resources находятся две папки, в них два файла которые содержат примеры данных, которые нужно обрабатывать.
 * Данные записаны в формате одна строка - одно поле. Название поля и значение разделяются символом ":".
 * Над некоторыми полями стоят аннотации, которые служат для проверки даннных поля. Подробное описание доступно в классе аннотации.
 * В отличии от изначального варианта, здесь на вход подается путь к ДИРЕКТОРИИ. То есть одна директория - один обьект (из файлов в этой директории)
___________________
 * Напишите код, который превратит содержимое папок в обьекты класса "Factory", учитывая аннотации над полями.
 * Парсинг должен учитывать следующие моменты:
 * 1. Парсятся значения только известных полей (которые есть в классе или упомянуты в аннотациях), все остальное игнорируется
 * 2. "null" должен распознаваться как null
 * 3. Ограничения аннотаций должны обрабатываться с помощью рефлексии
 * 4. Если файлы не прошли проверку, должна формироваться ошибка с информативным содержанием (см. ниже)
___________________
 Обработка ошибок:
 Если при попытке парсинга файла возникли ошибки проверки (не соблюдаются ограничения аннотаций), должно выбрасываться FactoryParsingException.
 Внутри должны быть перечислены все поля, при обработке которых возникли ошибки, и описание ошибки (напр. "Поле не может быть пустым")
 См. класс "FactoryParsingException" для доп. информации.
___________________
Обработка аннотаций и формирование ошибок оцениваются отдельно.
 * */

import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

public class FactoryParsingServiceImpl implements FactoryParsingService {

    Map<String, String> mapFieldValue = new HashMap<>();

    private void parseFile(String factoryDataDirectoryPath) {
        File dir = new File(factoryDataDirectoryPath); //path указывает на директорию
        List<File> files = Arrays.stream(dir.listFiles()).collect(Collectors.toList());

        Scanner scanner;
        for (File file : files) {
            try {
                scanner = new Scanner(file);
                while (scanner.hasNext()) {
                    String line = scanner.nextLine();
                    if (!line.contains("---")) {
                        String key = line.split(":")[0].replaceAll("\"", "").trim();
                        String value = line.split(":")[1].replaceAll("\"", "").trim();
                        mapFieldValue.put(key, value);
                    }
                }
            } catch (FileNotFoundException e) {
                System.out.println("В указанной директории нет файлов");
            }
        }
    }

    public static void main(String[] args) {
        FactoryParsingServiceImpl factoryParsingService = new FactoryParsingServiceImpl();
        String[] str = new String[2];
        str[0]="s1";
        str[1]="d2";
        System.out.println(factoryParsingService.concatenate(str, "*"));
    }

    String concatenate(String[] fields, String delim) throws NullPointerException{
        String result = "";
        String s = Arrays.asList(fields).toString();
        String substr = s.substring(1,s.length()-1);
        String[] split = substr.split(", ");
        int length = split.length;
        for (String field : split) {
            result = result.concat(mapFieldValue.get(field));
            length--;
            if (length>0)
                result = result.concat(delim);
        }
        return result;
    }

    void setVal(Field field, Factory clazz) throws IllegalAccessException {
        String s = mapFieldValue.get(field.getName());
        if (s.isEmpty() || s.equals("null"))
            field.set(clazz, null);

        else if (s.contains("[")) {
            s = s.substring(1, s.length() - 1);
            List<String> strings = new ArrayList<>(Arrays.asList(s.split(", ")));
            field.set(clazz, strings);
        }
        else field.set(clazz, s);
    }

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) {
            //write your code here
        parseFile(factoryDataDirectoryPath);

        ArrayList<FactoryParsingException.FactoryValidationError> errors = new ArrayList<>();

        try {
            Constructor<Factory> constructor = Factory.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            Factory factory = constructor.newInstance();

            Field[] declaredFields = Factory.class.getDeclaredFields();
            for (Field field : declaredFields) {
                field.setAccessible(true);
                NotBlank notBlankAnn = field.getAnnotation(NotBlank.class);
                Concatenate concatenateAnn = field.getAnnotation(Concatenate.class);

                if (concatenateAnn!=null){
                    try {
                        concatenate(concatenateAnn.fieldNames(), concatenateAnn.delimiter());
                    }catch (Exception e){
                        errors.add(new FactoryParsingException.FactoryValidationError(field.getName(),
                                "Нет полей  названиями из аннотации"));
                    }
                }else setVal(field, factory);
            }
            if (errors.size()>0){
                FactoryParsingException factoryParsingException = new FactoryParsingException("Список ошибок:", errors);
            }

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


        return null;
        }
}
